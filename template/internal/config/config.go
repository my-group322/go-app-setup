package config

import (
	"os"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

const envViperConfigFile = "VIPER_FILE"

type Config struct {
	URL                string `mapstructure:"url"`
	DBUrl              string `mapstructure:"db_url"`
	RedisURL           string `mapstructure:"redis"`
	JwtSecret          string `mapstructure:"jwt_secret"`
	NewRelicLicenceKey string `mapstructure:"new_relic_licence_key"`
}

func New(log *logrus.Logger) (*Config, error) {
	var config Config

	viperFilePath := os.Getenv(envViperConfigFile)
	if viperFilePath == "" {
		return nil, errors.New("failed to figure out viper file path")
	}

	viper.SetConfigFile(viperFilePath)

	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "failed to read config")
	}

	if err := viper.Unmarshal(&config); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal config")
	}

	if config.JwtSecret == "" {
		log.Warn("You are starting with empty jwt-secret value")
	}

	return &config, config.validate()
}

func (c Config) validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.URL, validation.Required),
		validation.Field(&c.DBUrl, validation.Required),
		validation.Field(&c.RedisURL, validation.Required),
	)
}
