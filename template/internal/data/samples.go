package data

import (
	"context"
)

//go:generate mockery --case underscore --name "SampleQ|SampleQCloner" --outpkg datamock

type SampleQ interface {
	Delete(id string) error
}

type SampleQCloner interface {
	Clone(ctx context.Context) SampleQ
}

type Sample struct {
	ID string
}
