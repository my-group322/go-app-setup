-- +migrate Up

create table sample (
    id uuid primary key,
);

-- +migrate Down

drop table sample cascade;
