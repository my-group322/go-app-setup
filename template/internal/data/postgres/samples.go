package postgres

import (
	"context"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"

	nrcontext "github.com/ilyasiv2003/newrelic-context"

	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/data"
	"gorm.io/gorm"
)

type sampleQ struct {
	db *gorm.DB
}

func newSampleQ(db *gorm.DB) *sampleQ {
	return &sampleQ{
		db: db,
	}
}

func NewSampleQCloner(db *gorm.DB) data.SampleQCloner {
	return newSampleQ(db)
}

func (q *sampleQ) Clone(ctx context.Context) data.SampleQ {
	return newSampleQ(nrcontext.SetTxnToGorm(ctx, q.db))
}

func (q *sampleQ) Delete(id string) error {
	return pkgdata.ErrorFromDelete(q.db.Delete(&data.Sample{ID: id}))
}
