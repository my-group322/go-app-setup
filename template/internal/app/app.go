package app

import (
	"context"
	"flag"

	"github.com/pkg/errors"
	migrate "github.com/rubenv/sql-migrate"
	"github.com/sirupsen/logrus"

	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/config"
	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/data/migrations"
	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/service"
	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/service/app"
)

func Start() bool {
	cfg, err := config.New(logrus.New())
	if err != nil {
		panic(errors.Wrap(err, "failed to setup config"))
	}

	a := app.New(cfg)

	migrateDown := flag.Bool("md", false, "migrate down")
	flag.Parse()

	if *migrateDown {
		if err := migrations.Migrate(a.Log, a.DB, migrate.Down); err != nil {
			a.Log.Error(err)
			return false
		}
		return true
	}

	if err := migrations.Migrate(a.Log, a.DB, migrate.Up); err != nil {
		a.Log.Error(err)
		return false
	}

	if err := service.NewService(a).Run(context.Background()); err != nil {
		a.Log.Error(err)
		return false
	}

	return true
}
