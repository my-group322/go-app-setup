package api

import (
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"
)

var (
	ErrSampleNotFound  = renderer.Error{Message: "sample not found", StatusCode: http.StatusNotFound}
	ErrUserNotVerified = renderer.Error{Message: "user not verified", StatusCode: http.StatusForbidden}
)
