package api

import (
	"context"
	"errors"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"
)

func (a api) DeleteSample(ctx context.Context, id string) error {
	log := a.Log.WithField("sampleID", id)

	sampleQ := a.SampleQ.Clone(ctx)

	if err := sampleQ.Delete(id); err != nil {
		log.WithError(err).Error("failed to add friend")

		if errors.Is(err, pkgdata.ErrNotAffected) {
			return ErrSampleNotFound
		}

		return err
	}

	return nil
}
