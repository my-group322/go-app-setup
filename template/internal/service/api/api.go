package api

import (
	"context"

	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/service/app"
)

//go:generate mockery --case underscore --name API --inpackage

type API interface {
	DeleteSample(ctx context.Context, id string) error
}

type api struct {
	*app.App
}

func New(app *app.App) API {
	return &api{app}
}
