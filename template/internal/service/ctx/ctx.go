package ctx

import (
	"context"
)

type ctxKey int

const (
	requesterCtxKey ctxKey = iota
)

func RequesterToCtx(id string) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, requesterCtxKey, id)
	}
}

func Requester(ctx context.Context) string {
	return ctx.Value(requesterCtxKey).(string)
}
