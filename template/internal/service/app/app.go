package app

import (
	"strings"

	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/data/redis"

	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/data"

	redislib "github.com/go-redis/redis/v8"
	"github.com/ilyasiv2003/newrelic-context/nrgorm"
	newrelic "github.com/newrelic/go-agent"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	driver "gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/config"
	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/data/postgres"
)

// App holds all main dependencies that have to be initialized on start
type App struct {
	Log     *logrus.Logger
	Cfg     *config.Config
	DB      *gorm.DB
	Metrics newrelic.Application
	SampleQ data.SampleQCloner
	Redis   *redislib.Client
	RedisQ  data.RedisQCloner
}

func New(cfg *config.Config) *App {
	var err error

	a := &App{
		Log: logrus.New(),
		Cfg: cfg,
	}

	a.DB, err = gorm.Open(driver.Open(cfg.DBUrl), &gorm.Config{SkipDefaultTransaction: true})
	if err != nil {
		panic(errors.Wrap(err, "failed to open database"))
	}
	nrgorm.AddGormCallbacks(a.DB)

	a.SampleQ = postgres.NewSampleQCloner(a.DB)

	a.Redis = redislib.NewClient(&redislib.Options{
		Addr:     cfg.RedisURL,
		Password: "",
		DB:       0,
	})

	a.RedisQ = redis.NewRedisQ(a.Redis)

	if a.Cfg.NewRelicLicenceKey == "" {
		a.Log.Warn("metrics are disabled")

		// Disabling app with invalid key
		a.Cfg.NewRelicLicenceKey = strings.Repeat("a", 40)
	}
	a.Metrics, err = newrelic.NewApplication(newrelic.NewConfig("pics-svc", cfg.NewRelicLicenceKey))
	if err != nil {
		panic(errors.Wrap(err, "failed to setup metrics"))
	}

	return a
}
