package http

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	nrcontext "github.com/ilyasiv2003/newrelic-context"
	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/service/app"
	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/service/http/middlewares"
	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/service/http/server"
)

func Router(a *app.App, server *server.Server) chi.Router {
	r := chi.NewRouter()

	r.Use(
		middleware.Heartbeat("/ping"),
		nrcontext.NewMiddlewareWithApp(a.Metrics).Handler,
		middlewares.AuthenticationMiddleware(a),
		middleware.Recoverer,
	)

	r.Get("/sample/{id}", server.GetSample)

	return r
}
