package server

import (
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"github.com/go-chi/chi/v5"
)

func (s Server) GetSample(w http.ResponseWriter, r *http.Request) {
	if err := s.api.DeleteSample(r.Context(), chi.URLParam(r, "id")); err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}
}
