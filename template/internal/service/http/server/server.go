package server

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/service/api"
)

type Server struct {
	api api.API
	log *logrus.Logger
}

func New(api api.API, log *logrus.Logger) *Server {
	return &Server{api, log}
}
