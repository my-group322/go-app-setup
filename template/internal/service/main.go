package service

import (
	"context"
	"net/http"

	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/service/api"
	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/service/app"
	myhttp "gitlab.com/my-group322/pictures/{{ AppName }}/internal/service/http"
	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/service/http/server"

	"github.com/pkg/errors"
)

type Service struct {
	app *app.App
}

func NewService(app *app.App) *Service {
	return &Service{app}
}

func (s *Service) Run(ctx context.Context) error {
	a := s.app

	newAPI := api.New(a)

	newServer := server.New(newAPI, a.Log)

	r := myhttp.Router(a, newServer)

	listener := &http.Server{Addr: a.Cfg.URL, Handler: r}

	errChan := make(chan error)
	go func() {
		if err := listener.ListenAndServe(); err != nil {
			errChan <- errors.Wrap(err, "router crashed")
		}
	}()

	select {
	case err := <-errChan:
		return err
	case <-ctx.Done():
		// Parent cancelled context, shutting down...
		if err := listener.Shutdown(context.Background()); err != nil {
			return err
		}
		return nil
	}
}
