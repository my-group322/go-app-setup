package resources

import jares "gitlab.com/my-group322/json-api-shared"

type Sample struct {
	jares.Key
	Attributes    SampleAttributes    `json:"attributes"`
	Relationships SampleRelationships `json:"relationships"`
}

type SampleAttributes struct {
	Attribute     string `json:"attribute"`
}

type SampleRelationships struct {
	Related jares.RelationCollection `json:"friends"`
}

type SampleResponse struct {
	Data     Sample     `json:"data"`
	Included jares.Included `json:"included"`
}

type SampleListResponse struct {
	Data []Sample `json:"data"`
}
