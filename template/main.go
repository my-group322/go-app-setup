package main

import (
	"os"

	"gitlab.com/my-group322/pictures/{{ AppName }}/internal/app"
)

func main() {
	if !app.Start() {
		os.Exit(1)
	}
}
