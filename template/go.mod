module gitlab.com/my-group322/pictures/{{ AppName }}

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/go-redis/redis/v8 v8.11.4
	github.com/ilyasiv2003/newrelic-context v0.0.0-20211203210028-6b947dffac58
	github.com/newrelic/go-agent v3.15.2+incompatible
	github.com/pkg/errors v0.9.1
	github.com/rubenv/sql-migrate v0.0.0-20211023115951-9f02b1e13857
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.9.0
	gitlab.com/my-group322/json-api-shared v0.0.0-20211208100130-285519047ba1
	gitlab.com/my-group322/pictures/auth-svc v0.0.0-20211208104333-3371184b1e64
	gorm.io/driver/postgres v1.2.3
	gorm.io/gorm v1.22.4
)
