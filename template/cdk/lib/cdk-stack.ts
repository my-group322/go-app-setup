import * as cdk from '@aws-cdk/core'

export class CdkStack extends cdk.Stack {
  props: any

  constructor(scope: cdk.Construct, id: string, props: any, stackProps?: cdk.StackProps) {
    super(scope, id, stackProps)

    this.props = props

  }
}
