#!/bin/sh

which curl || (apt-get update -y && apt-get install curl -y) > /dev/null 2>&1

curl --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" -X POST \
    "https://gitlab.com/api/v4/projects?visibility=public&name=$APP_NAME&namespace_id=$NAMESPACE_ID"

cd ./"$APP_NAME" || exit
git init --initial-branch=master
git remote add origin git@gitlab.com:my-group322/pictures/"$APP_NAME".git
git add .
git commit -m "Initial commit"
git push origin master
