#!/bin/sh

mkdir "$APP_NAME"
cp -r ./template/. ./"$APP_NAME"

# shellcheck disable=SC2038
find ./"$APP_NAME" -type f | xargs sed -i "s/{{ AppName }}/$APP_NAME/g"
