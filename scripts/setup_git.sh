#!/bin/sh

mkdir -p ~/.ssh
cat "$RSA_PRIVATE_KEY" > "$HOME/.ssh/id_rsa"
chmod 600 ~/.ssh/id_rsa

which ssh-agent || (apt-get update -y && apt-get install openssh-client git -y) > /dev/null 2>&1
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa > /dev/null 2>&1
(ssh-keyscan -H "$CI_SERVER_HOST" >> ~/.ssh/known_hosts) > /dev/null 2>&1

touch ~/.gitconfig
git config --global user.name "$GITLAB_USER_NAME"
git config --global user.email "$GITLAB_USER_EMAIL"